#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#define YYSTYPE double

#include <sstream>
#include <memory>
#include <vector>
#include <cstring>
#include "doctest.h"
#include "expr_parser.h"
#include <iostream>

using namespace std;

const char *genError = "x";
const char *test1 = "for(int i=0; x < 10; x++){int y = 1;}";
const char *test2 = "include \"doctest.h \" \n bool test(param1, param2){int test = 10;}";
const char *test3 = "include \"doctest.h \"";
const char *test4 = "return 1+2;";
const char *test5 = "do{int x=2;}while(test){test++;}";
const char *test6 = "int x=2, y=3;";
const char *test7 = "test(\"10\")";
const char *test8 = "test(10, test)";
const char *test9 = "test(param1, param2)";
const char *test10 = "bool test(param1, param2){int test = 10;}";
const char *test11 = "int test(param1, param2){int test = 10;}";
const char *test12 = "char test(hola){int test = 10;}";
const char *test13 = "void test(adios){int test = 10;}";
const char *test14 = "break;int test = 10;break;";
const char *test15 = "while(!test){test++;break;}";
const char *test16 = "if(!test){test++;}";
const char *test17 = "if(test == (1+2)){test++;}";
const char *test18 = "if(test <= 1 && test <= 1){test = 10 + 10;}";
const char *test19 = "if(test <= 1){test = 10 + 10;}";
const char *test20 = "test--;";
const char *test21 = "test = 10 + 10;";
const char *test22 = "int test = 10 + 10;";
const char *test23 = "int test = test2();";
const char *test24 = "bool test(param1, param2){int test = 10;}";
const char *test25 = "if(!test){test++;}else{test--}";
const char *test26 = "if(!test){test++;}else if(test){test--}";

bool genExceptionOnError() {
    std::istringstream in;

    in.str(genError);
    ExprLexer el(in);
    Parser ep(el);
    
    bool res;
    try {
        ep.parse();
        res = false;
    } catch (...) {
        res = true;
    }
    return res;
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test1);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test2);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test3);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test4);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test5);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test6);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test7);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test8);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test9);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test10);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test11);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test12);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test13);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test14);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test15);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test16);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test17);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test18);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test19);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test20);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test21);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test22);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test23);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test24);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test25);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

TEST_CASE("Add expression") {
    REQUIRE(genExceptionOnError());    
    std::istringstream in;
    in.str(test26);
    ExprLexer l(in);
    Parser p(l);
    
    bool parseSuccess = false;
    int exprCount = 0;
    try {
        exprCount = p.parse();
        parseSuccess = true;
    } catch (...) {
        parseSuccess = false;
    }
    REQUIRE(p.getCurrentToken() == Token::Eof);
    CHECK(parseSuccess);
}

