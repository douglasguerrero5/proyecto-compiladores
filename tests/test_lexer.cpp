#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <sstream>
#include <memory>
#include <cstring>
#include "doctest.h"
#include "expr_lexer.h"

const char *test1 = "45 + 10";
const char *test2 = "_var_1 + _var_2";
const char *test4 = "(0.56 + 1.54) - (10.34 + 333.789);";
const char *test5 = "//Line comment\n"
                    "45 // Line Comment\n"
                    "+ // Line Comment\n"
                    "10 // Line Comment\n"
                    "//";

const char *test6 = "/* Block comment */\n"
                    "/* Block comment */ 45 /* Block comment */ + /* Block comment */ 10\n";

const char *test7 = "/* Block comment ////// \n"
                    " Block comment *** /// */45/* Block comment ////// \n"
                    " Block comment *** /// */ + 10/* Block comment ////// \n"
                    " Block comment *** /// */";

const char *test8 = "int variable1 = 10; \n";

const char *test9 = "if(variable1 >= 5){ \n"
                    " variable1 += 20; \n"
                    " }";

const char *test10 = "while(true){ \n"
                    " variable1 += 20; \n"
                    " }";

const char *test11 = "for(int i = 0; i < variable1; i++){ \n"
                    " variable1 += 20; \n"
                    " break; \n"
                    " }";

const char *test12 = "function test(int variable1, int variable2){ \n"
                    " return variable1 + variable2; \n"
                    " }";

const char *test13 = "int variable1[3] = {10, 20, 30}; \n";

TEST_CASE("Add expr with numbers") {
    std::istringstream in;

    in.str(test1);
    ExprLexer l(in);
    Token tk = l.getNextToken();
    
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Add expr with variables") {
    std::istringstream in;

    in.str(test2);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Big buffer") {
    std::istringstream in;
    const int sizeKb = 64;
    int lastPos = sizeKb * 1024 - 1;
    std::unique_ptr<char[]> buff(new char[sizeKb * 1024]);
    char *p = buff.get();

    memset(p, ' ', sizeKb * 1024);
    int pos = lastPos - strlen(test1);
    strcpy(&p[pos], test1);
    p[lastPos] = '\0';

    in.str(p);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Complex expression with real numbers") {
    std::istringstream in;

    in.str(test4);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::OpenPar );
    CHECK( l.getText() == "(" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "0.56" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    CHECK( l.getText() == "+" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "1.54" );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    CHECK( l.getText() == ")" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpSub );
    CHECK( l.getText() == "-" );

    tk = l.getNextToken();
    CHECK( tk == Token::OpenPar );
    CHECK( l.getText() == "(" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "10.34" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    CHECK( l.getText() == "+" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "333.789" );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    CHECK( l.getText() == ")" );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    CHECK( l.getText() == ";" );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Line comments") {
    std::istringstream in;

    in.str(test5);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Num );
    CHECK( l.getText() == "45" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    CHECK( l.getText() == "+" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "10" );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Block comments 1") {
    std::istringstream in;

    in.str(test6);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Num );
    CHECK( l.getText() == "45" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    CHECK( l.getText() == "+" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "10" );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Block comments 2") {
    std::istringstream in;

    in.str(test7);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Num );
    CHECK( l.getText() == "45" );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    CHECK( l.getText() == "+" );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    CHECK( l.getText() == "10" );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Assign number to variable") {
    std::istringstream in;

    in.str(test8);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Int );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("if statement") {
    std::istringstream in;

    in.str(test9);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::If );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenPar );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::MoreThan );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("while statement") {
    std::istringstream in;

    in.str(test10);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::While );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenPar );
    tk = l.getNextToken();
    CHECK( tk == Token::True );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("for statement") {
    std::istringstream in;

    in.str(test11);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::For );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenPar );
    tk = l.getNextToken();
    CHECK( tk == Token::Int );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::LessThan );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::Break );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("function statement") {
    std::istringstream in;

    in.str(test12);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Function );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenPar );
    tk = l.getNextToken();
    CHECK( tk == Token::Int );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Comma );
    tk = l.getNextToken();
    CHECK( tk == Token::Int );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::ClosePar );
    tk = l.getNextToken();
    CHECK( tk == Token::OpenBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Return );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::OpAdd );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();
    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}

TEST_CASE("Assign numbers to array") {
    std::istringstream in;

    in.str(test13);
    ExprLexer l(in);
    Token tk = l.getNextToken();

    CHECK( tk == Token::Int );
    tk = l.getNextToken();
    CHECK( tk == Token::Id );
    tk = l.getNextToken();

    CHECK( tk == Token::OpenBracket );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBracket );
    tk = l.getNextToken();

    CHECK( tk == Token::Equals );
    tk = l.getNextToken();
    
    CHECK( tk == Token::OpenBrace );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Comma );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::Comma );
    tk = l.getNextToken();
    CHECK( tk == Token::Num );
    tk = l.getNextToken();
    CHECK( tk == Token::CloseBrace );
    tk = l.getNextToken();

    CHECK( tk == Token::Semicolon );
    tk = l.getNextToken();
    CHECK( tk == Token::Eof );
}
