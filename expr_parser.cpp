#include "expr_parser.h"
#include <iostream>

using namespace std;

int Parser::parse() {
    exprCount = 0;
    token = lexer.getNextToken();
    Block();
    return exprCount;
}

void Parser::Block(){
    if(token == Token::Int || token == Token::Char || token == Token::Bool || token == Token::Void){
        token = lexer.getNextToken();
        token = lexer.getNextToken();
        Assignment();
    }else if(token == Token::Id){
        token = lexer.getNextToken();
        Assignment();
    }else if(token == Token::Include){
        token = lexer.getNextToken();
        if(token == Token::Id){
            token = lexer.getNextToken();
            Block();
        }
    }else if(token == Token::Do){
        token = lexer.getNextToken();
            if(token == Token::OpenBrace){
            token = lexer.getNextToken();
            Block();
            if(token != Token::CloseBrace){
            }
            token = lexer.getNextToken();
            if(token == Token::While){
                token = lexer.getNextToken();
                Statement();
            }
    }
    }else if(token == Token::If || token == Token::While){
        token = lexer.getNextToken();
        Statement();
    }else if(token == Token::For){
        token = lexer.getNextToken();
        For();
    }else if(token == Token::Break || token == Token::Continue){
        token = lexer.getNextToken();
        if(token != Token::Semicolon){

        }
        token = lexer.getNextToken();
        Block();
    }else if(token == Token::Return){
        token = lexer.getNextToken();
        Stmt();
    }
}

///////////////////

void Parser::For(){
    ForCondition();
    if(token == Token::OpenBrace){
        token = lexer.getNextToken();
        Block();
        if(token != Token::CloseBrace){
        }
        token = lexer.getNextToken();
    }
}

void Parser::ForCondition(){
    EvaluateFor();
    ForConditionP();
}

void Parser::ForConditionP(){
    if(token == Token::LessThan){
        token = lexer.getNextToken();
        ForCondition();
    }else if(token == Token::Semicolon){
        token = lexer.getNextToken();
        ForCondition();
    }else if(token == Token::OpAdd){
        token = lexer.getNextToken();
        if(token == Token::OpAdd){
            token = lexer.getNextToken();
            StmtP();
        } 
    }
}

void Parser::EvaluateFor(){

    if(token == Token::Id || token == Token::Num){
        token = lexer.getNextToken();
    }else if(token == Token::Int){
        token = lexer.getNextToken();
        token = lexer.getNextToken();
        Assignment();
    }else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        ForCondition();
        if(token != Token::ClosePar){
            throw "Error";
        }
        token = lexer.getNextToken();
    }else {
        throw "Error";
    } 
}

//////////////////////////////////////////////////////

void Parser::Function(){
    Parameter();
    if(token == Token::OpenBrace){
        token = lexer.getNextToken();
        Block();
        if(token != Token::CloseBrace){
        }
        token = lexer.getNextToken();
    }
}

void Parser::Parameter(){
    EvaluateParameter();
    ParameterP();
}

void Parser::ParameterP(){
    if(token == Token::Comma){
        token = lexer.getNextToken();
        Parameter();
    } 
}

void Parser::EvaluateParameter(){
    if(token == Token::Id || token == Token::Num){
        token = lexer.getNextToken();  
    }else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        if(token == Token::ClosePar){
            token = lexer.getNextToken();
            return;
        }
        Parameter();
        if(token != Token::ClosePar){
            throw "Error";
        }
        token = lexer.getNextToken();
    }
    else {
        throw "Error";
    } 
}

void Parser::Statement(){
    Condition();
    if(token == Token::OpenBrace){
        token = lexer.getNextToken();
        Block();
        if(token != Token::CloseBrace){
        }
        token = lexer.getNextToken();

        if(token == Token::Else){
            token = lexer.getNextToken();
            if(token == Token::OpenBrace){
                token = lexer.getNextToken();
                Block();
            }else if(token == Token::If){
                Block();
            }
            if(token != Token::CloseBrace){
            }
            token = lexer.getNextToken();        
        }
    }
}

void Parser::Condition(){
    EvaluateStatement();
    ConditionP();
}

void Parser::ConditionP(){
    if(token == Token::Equals || token == Token::MoreThan || token == Token::LessThan){
        token = lexer.getNextToken();
        if(token == Token::Equals){
            token = lexer.getNextToken();
            if(token == Token::OpenPar){
                Expr();
            }else{
                Condition();
            }
        } 
    } 
}

void Parser::EvaluateStatement(){
    if(token == Token::Id || token == Token::Num){
        token = lexer.getNextToken();
        if(token == Token::Ampersand){
            token = lexer.getNextToken();
            if(token == Token::Ampersand){
                token = lexer.getNextToken();
                Condition();
            }
        }else if(token == Token::VerticalLine){
            token = lexer.getNextToken();
            if(token == Token::VerticalLine){
                token = lexer.getNextToken();
                Condition();
            }
        }
    }else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        Condition();
        if(token != Token::ClosePar){
            throw "Error";
        }
        token = lexer.getNextToken();
    }else if(token == Token::True || token == Token::False){
        token = lexer.getNextToken();
    }else if(token == Token::Exclamation){
        token = lexer.getNextToken();
        Condition();
    }else if(token == Token::Eof){
        return;
    }
    else {
        throw "Error";
    } 
}

void Parser::Assignment(){
    if(token == Token::Equals){
        token = lexer.getNextToken();
        Stmt();
    }else if(token == Token::OpAdd){
        token = lexer.getNextToken();
        if(token == Token::OpAdd){
            token = lexer.getNextToken();
            StmtP();
        }else if(token == Token::Equals){
            token = lexer.getNextToken();
            Stmt();
        }
    }else if(token == Token::OpSub){
        token = lexer.getNextToken();
        if(token == Token::OpSub){
            token = lexer.getNextToken();
            StmtP();
        }else if(token == Token::Equals){
            token = lexer.getNextToken();
            Stmt();
        }
    }else if(token == Token::OpMul){
        token = lexer.getNextToken();
        if(token == Token::Equals){
            token = lexer.getNextToken();
            Stmt();
        }
    }else if(token == Token::OpenPar){
        Function();
    }else if(token == Token::Eof){
        throw "Error";
    }
}

void Parser::Stmt(){
    Expr();
    StmtP();
}

void Parser::StmtP(){
    if(token == Token::Semicolon){
        token = lexer.getNextToken();
        Block();
    }else if(token == Token::Comma){
        token = lexer.getNextToken();
        Block();
    }
}

void Parser::Expr(){
    Term();
    ExprP();
}

void Parser::ExprP(){
    if(token == Token::OpAdd || token == Token::OpSub){
        token = lexer.getNextToken();
        Term();
        ExprP();
    } 
}

void Parser::Term(){
    Factor();
    TermP();
}

void Parser::TermP(){
    if(token == Token::OpDiv || token == Token::OpMul){
        token = lexer.getNextToken();
        Term();
        ExprP();
    } 
}

void Parser::Factor(){
    if(token == Token::Num){
        token = lexer.getNextToken();
    }
    else if(token == Token::Id){
        token = lexer.getNextToken();
        if(token == Token::OpenPar){
            Function();
        }
    }
    else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        Expr();
        if(token != Token::ClosePar){
        }
        token = lexer.getNextToken();
    }else if(token == Token::CloseBrace){
        return;
    }else if(token == Token::Semicolon){
        token = lexer.getNextToken();
        Block();
    }else if(token == Token::Eof){
        return;
    }
    else {
        throw "Error";
    }    
}
