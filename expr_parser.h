#ifndef _PARSER_H
#define _PARSER_H

#include "expr_lexer.h"

class Parser {
public:
    Parser(ExprLexer& lexer): lexer(lexer) {}
    int parse();
    Token getCurrentToken(){ return token; }

private:
    ExprLexer& lexer;
    Token token;
    int exprCount;

private:
    void Block();
    void Assignment();
    void Statement();
    void For();
    void Function();

    void Stmt();
    void StmtP();
    void Expr();
    void ExprP();
    void Term();
    void TermP();
    void Factor();  

    void EvaluateFor();
    void ForCondition();
    void ForConditionP();

    void EvaluateParameter();
    void Parameter();
    void ParameterP();

    void EvaluateStatement();
    void Condition();
    void ConditionP();
};

#endif
