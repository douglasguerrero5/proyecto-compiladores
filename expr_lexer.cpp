#include "expr_lexer.h"

void ExprLexer::ungetChar(){
    in.unget();
}

char ExprLexer::getNextChar(){
    return in.get();
}

Token ExprLexer::getNextToken() {
    text="";
    while(1){
        c = getNextChar();
        if(c == ' ' || c == '\t' || c == '\n' ){
            continue;
        }else if(c == '/'){
            c = getNextChar();
            if(c == '*'){
                c = getNextChar();
                bool endOfBlock = false;
                while(!endOfBlock){
                    c = getNextChar();
                    if(c == '*'){
                        c = getNextChar();
                        if(c == '/'){
                            endOfBlock = true;
                            c = getNextChar();
                        }
                    }
                }
                ungetChar();
            }else {
                c = getNextChar();
                while(c != '\n' && c != EOF){
                    c = getNextChar();
                }
                ungetChar();
            }
        }else if(c == ','){
            text += c;
            return Token::Comma;
        }else if(c == '<'){
            text += c;
            return Token::LessThan;
        }else if(c == '>'){
            text += c;
            return Token::MoreThan;
        }else if(c == '&'){
            text += c;
            return Token::Ampersand;
        }else if(c == '|'){
            text += c;
            return Token::VerticalLine;
        }else if(c == '!'){
            text += c;
            return Token::Exclamation;
        }else if(c == '='){
            text += c;
            return Token::Equals;
        }else if(c == '%'){
            text += c;
            return Token::Percent;
        }else if(c == '{'){
            text += c;
            return Token::OpenBrace;
        }else if(c == '}'){
            text += c;
            return Token::CloseBrace;
        }else if(c == '['){
            text += c;
            return Token::OpenBracket;
        }else if(c == ']'){
            text += c;
            return Token::CloseBracket;
        }else if(c == '('){
            text += c;
            return Token::OpenPar;
        }else if(c == ')'){
            text += c;
            return Token::ClosePar;
        }else if(c == ';'){
            text += c;
            return Token::Semicolon;
        }else if(c == '+'){
            text += c;
            return Token::OpAdd;
        }else if(c == '-'){
            text += c;
            return Token::OpSub;
        }else if(c == '/'){
            text += c;
            return Token::OpDiv;
        }else if(c == '*'){
            text += c;
            return Token::OpMul;
        }else if(isdigit(c)){
            text += c;
            while(isdigit(c) || c == '.'){
                c = getNextChar();
                text += c;
            }
            text = text.substr(0, text.size()-1);
            ungetChar();
            return Token::Num;
        }else if(isalpha(c) || c == '_'){
            text += c;
            //revisar que tenga char despues de . o hacer nuevo token como file
            while(isalnum(c) || c == '_' || c == '.'){
                c = getNextChar();
                text += c;
            }
            ungetChar();
            text = text.substr(0, text.size()-1);
            return getIdentType(text);
        }else if(c == EOF){
            return Token::Eof;
        }
    }
    return Token::Error;
}

Token ExprLexer::getIdentType(std::string text){
    if(text == "int"){
        return Token::Int;
    }else if(text == "bool"){
        return Token::Bool;
    }else if(text == "char"){
        return Token::Char;
    }else if(text == "void"){
        return Token::Void;
    }else if(text == "function"){
        return Token::Function;
    }else if(text == "while"){
        return Token::While;
    }else if(text == "do"){
        return Token::Do;
    }else if(text == "for"){
        return Token::For;
    }else if(text == "if"){
        return Token::If;
    }else if(text == "else"){
        return Token::Else;
    }else if(text == "break"){
        return Token::Break;
    }else if(text == "continue"){
        return Token::Continue;
    }else if(text == "include"){
        return Token::Include;
    }else if(text == "true"){
        return Token::True;
    }else if(text == "false"){
        return Token::False;
    }else if(text == "return"){
        return Token::Return;
    }else{
        return Token::Id;
    }
}

std::string ExprLexer::getTokenText(Token token){
    switch(token){
        case Token::Int:
            return "Int";
            break;
        case Token::Char:
            return "Char";
            break;
        case Token::Void:
            return "Char";
            break;
        case Token::Function:
            return "Function";
            break;
        case Token::Bool:
            return "Bool";
            break;
        case Token::Id:
            return "Id";
            break;
        case Token::OpMul:
            return "Multi";
            break;
        case Token::OpAdd:
            return "Add";
            break;
        case Token::OpSub:
            return "Subtract";
            break;
        case Token::Num:
            return "Number";
            break;
        case Token::Eof:
            return "EOF";
            break;
        case Token::OpDiv:
            return "Divide";
            break;
        case Token::OpenPar:
            return "OpenParenthesis";
            break;
        case Token::ClosePar:
            return "CloseParenthesis";
            break;
        case Token::Equals:
            return "Equals";
            break;
        case Token::Semicolon:
            return "Semicolon";
            break;
        case Token::OpenBrace:
            return "OpenBrace";
            break;
        case Token::CloseBrace:
            return "CloseBrace";
            break;
        case Token::OpenBracket:
            return "OpenBracket";
            break;
        case Token::CloseBracket:
            return "CloseBracket";
            break;
        case Token::Break:
            return "Break";
            break;
        case Token::Continue:
            return "Continue";
            break;
        case Token::Return:
            return "Return";
            break;
        case Token::If:
            return "If";
            break;
        case Token::Else:
            return "Else";
            break;
        case Token::Include:
            return "Include";
            break;
        default: 
            return "Invalid";
            break;
    }
}