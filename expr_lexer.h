#ifndef _EXPR_LEXER_H
#define _EXPR_LEXER_H
#include <iostream>
#include <fstream>
#include <string>

enum class Token {
    Num         = 100,
    Id          = 101,
    OpAdd       = 102,
    OpSub       = 103,
    OpMul       = 104,
    OpenPar     = 105,
    ClosePar    = 106,
    OpDiv       = 107,
    Semicolon   = 108,
    Ampersand   = 109,
    Percent     = 110,
    Equals      = 111,
    Break       = 112,
    If          = 113,
    Continue    = 114,
    For         = 115,
    While       = 116,
    Int         = 117,
    Char        = 118,
    Function    = 119,
    Bool        = 120,
    OpenBracket = 121,
    CloseBracket= 122,
    OpenBrace   = 123,
    CloseBrace  = 124,
    LessThan    = 125,
    MoreThan    = 126,
    True        = 127,
    False       = 128,
    Return      = 129,
    Comma       = 130,
    VerticalLine= 131,
    Exclamation = 132,
    Void        = 133,
    Do          = 134,
    Include     = 135,
    Else        = 136,
    Error       = 998,
    Eof         = 999,
};

class ExprLexer {
public:
    ExprLexer(std::istream &in) : in(in) {}
    ~ExprLexer() {}

    Token getNextToken();
    std::string getText() { return text; }
    std::string getTokenText(Token token);

private: 
    char getNextChar();
    void ungetChar();
    Token getIdentType(std::string text);

private:
    std::istream &in;
    std::string text;
    char c;
};
#endif
